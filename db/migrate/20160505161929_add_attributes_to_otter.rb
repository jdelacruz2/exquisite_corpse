class AddAttributesToOtter < ActiveRecord::Migration
  def change
    add_column :otters, :name, :string
    add_column :otters, :cuteness_score, :int
    add_column :otters, :fish_intake, :int

    add_column :waves, :otter_id, :int
    add_column :waves, :color, :string
    add_column :waves, :key_id, :int

    add_column :keys, :wave_id, :int
    add_column :keys, :amplitude, :int
    add_column :keys, :shininess, :int
  end
end
