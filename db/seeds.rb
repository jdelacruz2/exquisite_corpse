# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#

otters = Otter.create([{ name: 'Fluffy', cuteness_score: 6, fish_intake: 10 }, { name: 'Slippy', cuteness_score: 2, fish_intake: 25 }, { name: 'Otty', cuteness_score: 17, fish_intake: 15 }])

keys = Key.create([{ wave_id: 1, amplitude: 31, shininess: 15 }, { wave_id: 3, amplitude: 21, shininess: 11 }, { wave_id: 2, amplitude: 44, shininess: 30 }])
#
waves = Wave.create([{ otter_id: 1, key_id: 2, color: 'blue' }, { otter_id: 2, key_id: 3, color: 'death' },{ otter_id: 3, key_id: 1, color: 'garbage pile' }])
#
# otters.each do |otter|
#   Otter.create(otter)
# end
#
# keys.each do |key|
#   Key.create(key)
# end
#
# waves.each do |wave|
#   Wave.create(wave)
# end
